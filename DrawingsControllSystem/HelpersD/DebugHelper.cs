﻿using System.Windows;

namespace DrawingsControllSystem.DEBUG
{
    public static class DebugHelper
    {
        public static void ShowInDialogDebug(this string s)
        {
            MessageBox.Show(s, "DEBUG Dialog", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}