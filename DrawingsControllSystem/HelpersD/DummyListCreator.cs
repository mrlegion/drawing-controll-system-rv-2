﻿using System;
using System.Collections.Generic;
using System.IO;
using DrawingsControllSystem.ViewModels;

namespace DrawingsControllSystem.HelpersD
{
    public class DummyListCreator
    {
        private static Random _random = new Random();

        public DummyItem GenerateDummyItem()
        {
            int count = _random.Next(1, 20);
            int include = count * _random.Next(1, 16);

            return new DummyItem()
            {
                Count = count,
                Include = include,
                Name = Path.GetRandomFileName(),
                Path = $"C:\\Scan\\{ Path.GetRandomFileName() }"
            };
        }

        public List<DummyItem> GetListDummy(int count)
        {
            List<DummyItem> result = new List<DummyItem>();

            for (int i = 0; i < count; i++)
            {
                result.Add(GenerateDummyItem());
            }

            return result;
        }
    }
}