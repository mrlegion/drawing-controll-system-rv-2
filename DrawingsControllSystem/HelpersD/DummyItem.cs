﻿namespace DrawingsControllSystem.HelpersD
{
    public class DummyItem
    {
        public string Name { get; set; }

        public int Count { get; set; }

        public int Include { get; set; }

        public string Path { get; set; }
    }
}