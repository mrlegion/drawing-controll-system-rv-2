﻿using System;
using Unity;

namespace DrawingsControllSystem.Common.Archive
{
    public sealed class Container : IFContainer
    {
        private readonly IUnityContainer _container = new UnityContainer();

        public void RegisterType<TType>() where TType : class
        {
            _container.RegisterType<TType>();
        }

        public void RegisterType<TType, TConcrete>() where TConcrete : class, TType
        {
            _container.RegisterType<TType, TConcrete>();
        }

        public void RegisterSingleton<TType>() where TType : class
        {
            _container.RegisterSingleton<TType>();
        }

        public void RegisterSingleton<TType, TConcrete>() where TConcrete : class, TType
        {
            _container.RegisterSingleton<TType, TConcrete>();
        }


        // TODO: Подумать как реализовать
        //public static void RegisterInstance<TType>(TType instance) where TType : class
        //{
        //    container.RegisterInstance()
        //}

        //public static void RegisterInstance<TType, TConcrete>(TConcrete instance) where TConcrete : class, TType
        //{
        //}

        public TTypeToResolve Resolve<TTypeToResolve>()
        {
            return _container.Resolve<TTypeToResolve>();
        }

        public object Resolve(Type type)
        {
            return _container.Resolve(type);
        }
    }
}