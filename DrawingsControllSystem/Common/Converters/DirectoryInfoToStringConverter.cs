﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Data;

namespace DrawingsControllSystem.Common.Converters
{
    public class DirectoryInfoToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DirectoryInfo di)
            {
                return di.FullName;
            }

            return "Нажмите на кнопку Обзор, чтобы выбрать директорию";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}