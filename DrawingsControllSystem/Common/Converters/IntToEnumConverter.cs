﻿using System;
using System.Globalization;
using System.Windows.Data;
using DrawingsControllSystem.Model.Common;

namespace DrawingsControllSystem.Common.Converters
{
    public class IntToEnumConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var isEnum = value is FileExtansion;
            return (isEnum) ? (int) value : 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return FileExtansion.None;
            var obj = Enum.ToObject(typeof(FileExtansion), value);
            if (obj is FileExtansion fe)
                return fe;
            return FileExtansion.None;
        }
    }
}