﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Data;

namespace DrawingsControllSystem.Common.Converters
{
    public class DirectoryInfoToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string v = value as string;
            if (string.IsNullOrEmpty(v) || string.IsNullOrWhiteSpace(v))
                return false;
            return Directory.Exists(v);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}