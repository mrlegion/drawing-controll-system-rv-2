﻿using System.Collections.Generic;
using System.Linq;
using DrawingsControllSystem.Common;
using DrawingsControllSystem.Common.MVVM;
using DrawingsControllSystem.HelpersD;
using DrawingsControllSystem.ViewModels.UserComponents;
using Unity;
using Unity.Resolution;

namespace DrawingsControllSystem.ViewModels
{
    public class InfoViewModel : BindableBase
    {
        private List<DummyItem> _dummy;
        
        // DEBUG field
        private bool _isEnable = true;

        public InfoViewModel(IUnityContainer container)
        {
            BackDataContext = container.Resolve<BackPanelViewModel>();
            BackDataContext.Title = "Information Page";
            BackDataContext.Page = Pages.Search;

            var dummyCreator = container.Resolve<DummyListCreator>();
            Dummy = dummyCreator.GetListDummy(20);

            SwitchEnableDataGridCommand = new DelegateCommand( () => IsEmabled = !IsEmabled );
        }

        public BackPanelViewModel BackDataContext { get; }

        public List<DummyItem> Dummy
        {
            get => _dummy;
            set => SetProperty(ref _dummy, value);
        }

        public bool IsEmabled
        {
            get => _isEnable;
            set => SetProperty(ref _isEnable, value);
        }

        public string Count
        {
            get { return $"{_dummy.Sum(item => item.Count)} шт"; }
        }

        public string Include
        {
            get { return $"{_dummy.Sum(item => item.Include)} шт"; }
        }

        public DelegateCommand SwitchEnableDataGridCommand { get; }
    }
}