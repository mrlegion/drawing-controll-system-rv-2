﻿using System.IO;
using System.Runtime.Remoting.Messaging;
using System.Windows;
using DrawingsControllSystem.Common;
using DrawingsControllSystem.Common.MVVM;
using DrawingsControllSystem.Model;
using DrawingsControllSystem.Model.Common;
using DrawingsControllSystem.Model.Interfaces;
using DrawingsControllSystem.ViewModels.UserComponents;
using DrawingsControllSystem.Views;
using Microsoft.WindowsAPICodePack.Dialogs;
using Unity;
using Unity.Injection;
using Unity.Resolution;

namespace DrawingsControllSystem.ViewModels
{
    public class WatcherViewModel : BindableBase
    {
        #region Fields

        // Компоненты
        private readonly ILogger _logger;
        private readonly BaseController _controller;

        // Приватные переменные
        private bool _isRunning = false;
        
        #endregion

        #region Construct

        /// <summary>
        /// Инициализация нового класса <see cref="WatcherViewModel"/>
        /// </summary>
        /// <param name="container">IoC-контейнер приложения</param>
        public WatcherViewModel(IUnityContainer container)
        {
            _logger = container.Resolve<ILogger>();
            _controller = container.Resolve<BaseController>("WatcherController");

            BrowseDataContext = container.Resolve<BrowseViewModel>(new ResolverOverride[]
            {
                new ParameterOverride("logger", _logger),
            });

            LogDataContext = container.Resolve<LogViewModel>(new ResolverOverride[]
            {
                new ParameterOverride("logger", _logger),
                new ParameterOverride("controller", _controller), 
            });

            BackDataContext = container.Resolve<BackPanelViewModel>();
            BackDataContext.Title = "Watcher System Controll";
            BackDataContext.Page = Pages.Home;

            SelectTypeFileDataContext = container.Resolve<SelectTypeFileViewModel>(new ResolverOverride[]
            {
                new ParameterOverride("controller", _controller), 
            });

            _controller.PropertyChanged += (sender, args) => RaisePropertyChanged(args.PropertyName);
            BrowseDataContext.PropertyChanged += (sender, args) => RaisePropertyChanged(args.PropertyName);
            LogDataContext.PropertyChanged += (sender, args) => RaisePropertyChanged(args.PropertyName);
            SelectTypeFileDataContext.PropertyChanged += (sender, args) => RaisePropertyChanged(args.PropertyName);

            InitCommand();
        }

        #endregion

        #region Private Method

        private void InitCommand()
        {
            RunWatchCommand = new DelegateCommand(() =>
            {
                if (_controller.Extansion == FileExtansion.None)
                {
                    _logger.Logging("Не выбран тип ослеживаемых фалов!");
                    return;
                }

                _controller.Directory = BrowseDataContext.SelectDirectory;

                if (_controller.Directory != null && _controller.Directory.Exists)
                    IsRunning = !IsRunning;

                _controller.Run();
            });

            StopWatchCommand = new DelegateCommand(() =>
            {
                IsRunning = !IsRunning;
                _controller.Stop();
            });

            CloseApplicationCommand = new DelegateCommand(() =>
            {
                Application.Current.Shutdown();
            });

            ChangeChapterCommand = new DelegateCommand(() =>
            {
                _controller.ChangeFolder();
            });

            ExplodeCommand = new DelegateCommand(() => _controller.Explode());

            ShowInfoCommand = new DelegateCommand(() => _controller.ShowInformation());
        }

        #endregion

        #region Delegate Command properties

        public DelegateCommand CloseApplicationCommand { get; private set; }

        public DelegateCommand RunWatchCommand { get; private set; }

        public DelegateCommand StopWatchCommand { get; private set; }

        public DelegateCommand ChangeChapterCommand { get; private set; }

        public DelegateCommand ExplodeCommand { get; private set; }

        public DelegateCommand ShowInfoCommand { get; private set; }

        #endregion

        #region Properties

        public string Log => _logger.Log;

        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        public BrowseViewModel BrowseDataContext { get; }

        public LogViewModel LogDataContext { get; }

        public BackPanelViewModel BackDataContext { get; }

        public SelectTypeFileViewModel SelectTypeFileDataContext { get; }

        #endregion
    }
}