﻿using DrawingsControllSystem.Common.MVVM;
using DrawingsControllSystem.Model.Common;

namespace DrawingsControllSystem.ViewModels.UserComponents
{
    public class SelectTypeFileViewModel : BindableBase
    {
        private readonly BaseController _controller;

        public SelectTypeFileViewModel(BaseController controller)
        {
            _controller = controller;
            _controller.Extansion = FileExtansion.Tiff;
        }

        public FileExtansion Extansion
        {
            get => _controller.Extansion;
            set => _controller.Extansion = value;
        }
    }
}
