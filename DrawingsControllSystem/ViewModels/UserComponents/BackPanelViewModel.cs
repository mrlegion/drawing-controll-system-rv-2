﻿using System;
using System.Windows.Controls;
using DrawingsControllSystem.Common;
using DrawingsControllSystem.Common.MVVM;
using DrawingsControllSystem.Views;
using Unity;

namespace DrawingsControllSystem.ViewModels.UserComponents
{
    public class BackPanelViewModel : BindableBase
    {
        private string _title;
        private Pages _page;

        public BackPanelViewModel(IUnityContainer container)
        {
            Page = Pages.Home;

            ReturnCommand = new DelegateCommand(() =>
            {
                Page current;

                switch (Page)
                {
                    case Pages.Search:
                        current = container.Resolve<SearcherView>();
                        break;
                    case Pages.Watch:
                        current = container.Resolve<WatcherView>();
                        break;
                    case Pages.Home:
                    case Pages.None:
                    default:
                        current = container.Resolve<SelectView>();
                        break;
                }

                container.Resolve<ShellViewModel>().Current = current;
            });
        }

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        public Pages Page
        {
            get => _page;
            set => SetProperty(ref _page, value);
        }

        public DelegateCommand ReturnCommand { get; }
    }
}