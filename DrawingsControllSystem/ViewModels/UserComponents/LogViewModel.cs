﻿using System.IO;
using System.Windows;
using DrawingsControllSystem.Common.MVVM;
using DrawingsControllSystem.Model.Common;
using DrawingsControllSystem.Model.Interfaces;
using Microsoft.Win32;

namespace DrawingsControllSystem.ViewModels.UserComponents
{
    public class LogViewModel : BindableBase
    {
        private readonly ILogger _logger;

        public LogViewModel(ILogger logger, BaseController controller)
        {
            _logger = logger;
            _logger.PropertyChanged += (sender, args) => RaisePropertyChanged(args.PropertyName);

            SaveLogFileCommand = new DelegateCommand(() =>
            {
                if (controller.Directory == null)
                {
                    SaveFileDialog dialog = new SaveFileDialog()
                    {
                        Filter = "Текстовые файлы (.txt)|*.txt",
                        AddExtension = true,
                        FileName = "log",
                        DefaultExt = ".text",
                        Title = "Сохранение журнала событий:",
                    };

                    if (dialog.ShowDialog() == true)
                    {
                        DirectoryInfo directory = new DirectoryInfo(dialog.FileName);
                        _logger.SaveToFile(directory.Parent);
                        return;
                    }

                    _logger.Logging($"Не выбрана директория сохранения журнала событий!");
                    return;
                }
                controller?.SafeLogToFile();
            });

            CloseApplicationCommand = new DelegateCommand(() =>
            {
                Application.Current.Shutdown();
            });

            ShowInfoCommand = new DelegateCommand(() =>
            {
                if (controller.Directory == null)
                {
                    _logger.Logging("Не выбрана директория! Вывод информации не возможен!");
                    return;
                }

                controller?.ShowInformation();
            });
        }

        public DelegateCommand SaveLogFileCommand { get; }

        public DelegateCommand ShowInfoCommand { get; set; }

        public DelegateCommand CloseApplicationCommand { get; private set; }

        public string Log => _logger.Log;
    }
}