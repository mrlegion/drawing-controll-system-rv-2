﻿using System.IO;
using DrawingsControllSystem.Common.MVVM;
using DrawingsControllSystem.Model.Interfaces;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace DrawingsControllSystem.ViewModels.UserComponents
{
    public class BrowseViewModel : BindableBase
    {
        // Приватные переменные
        private DirectoryInfo _selectDirectory;
        private string _parrent;
        private readonly ILogger _logger;

        // Todo: может быть стоит передавать контроллер и устанавливать уже у него напрямую директорию
        public BrowseViewModel(ILogger logger)
        {
            _logger = logger;

            SelectDirectoryCommand = new DelegateCommand(() =>
            {
                var dialog = new CommonOpenFileDialog()
                {
                    Title = "Select Watching Directory",
                    Multiselect = false,
                    IsFolderPicker = true,
                    AllowNonFileSystemItems = true
                };

                if (!string.IsNullOrEmpty(_parrent))
                    dialog.InitialDirectory = _parrent;

                if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    SelectDirectory = new DirectoryInfo(dialog.FileName);
                }
            });
        }

        public DelegateCommand SelectDirectoryCommand { get; private set; }

        public DirectoryInfo SelectDirectory
        {
            get => _selectDirectory;
            set
            {
                _parrent = value.Parent?.FullName;
                SetProperty(ref _selectDirectory, value);
                _logger.Logging($"Установка новой директории для отслеживания: [ {SelectDirectory.FullName} ]");
            }
        }
    }
}