﻿using System.Windows;
using System.Windows.Controls;
using DrawingsControllSystem.Common;
using DrawingsControllSystem.Common.MVVM;
using DrawingsControllSystem.Views;
using Unity;

namespace DrawingsControllSystem.ViewModels
{
    public class SelectViewModel : BindableBase
    {

        public SelectViewModel(IUnityContainer container)
        {
            OpenWatchCommand = new DelegateCommand(() =>
            {
                Page page = container.Resolve<WatcherView>();
                page.DataContext = container.Resolve<WatcherViewModel>();
                container.Resolve<ShellViewModel>().Current = page;
            });

            OpenSearchCommand = new DelegateCommand(() =>
            {
                Page page = container.Resolve<SearcherView>();
                page.DataContext = container.Resolve<SearcherViewModel>();
                container.Resolve<ShellViewModel>().Current = page;
            });

            ExitApplicationCommand = new DelegateCommand(() => Application.Current.Shutdown());
        }

        public DelegateCommand OpenWatchCommand { get; }

        public DelegateCommand OpenSearchCommand { get; }

        public DelegateCommand ExitApplicationCommand { get; }
    }
}