﻿using System.Windows.Controls;
using CommonServiceLocator;
using DrawingsControllSystem.Common;
using DrawingsControllSystem.Common.MVVM;
using DrawingsControllSystem.Model.Common;
using DrawingsControllSystem.Model.Interfaces;
using DrawingsControllSystem.ViewModels.UserComponents;
using DrawingsControllSystem.Views;
using Unity;
using Unity.Resolution;

namespace DrawingsControllSystem.ViewModels
{
    public class SearcherViewModel : BindableBase
    {
        public SearcherViewModel(IUnityContainer container)
        {
            var controller = container.Resolve<BaseController>("SearchController");
            var logger = container.Resolve<ILogger>();
            BrowseDataContext = container.Resolve<BrowseViewModel>();
            LogDataContext = container.Resolve<LogViewModel>(new ResolverOverride[]
            {
                new ParameterOverride("logger", logger),
                new ParameterOverride("controller", controller),
            });

            BackDataContext = container.Resolve<BackPanelViewModel>();
            BackDataContext.Title = "Searcher System Controll";
            BackDataContext.Page = Pages.Home;

            SelectTypeFileDataContext = container.Resolve<SelectTypeFileViewModel>(new ResolverOverride[]
            {
                new ParameterOverride("controller", controller),
            });

            BrowseDataContext.PropertyChanged += (sender, args) => RaisePropertyChanged(args.PropertyName);
            LogDataContext.PropertyChanged += (sender, args) => RaisePropertyChanged(args.PropertyName);

            LogDataContext.ShowInfoCommand = new DelegateCommand(() =>
            {
                controller.ShowInformation();
                // Page info = container.Resolve<InfoView>();
                // info.DataContext = container.Resolve<InfoViewModel>();
                // container.Resolve<ShellViewModel>().Current = info;
            });

            // Инициализация команд
            RunScanCommand = new DelegateCommand(() =>
            {
                if (controller.Extansion == FileExtansion.None)
                {
                    logger.Logging($"Тип сканироваемых файлов не выбран!");
                    return;
                }

                if (BrowseDataContext.SelectDirectory == null || !BrowseDataContext.SelectDirectory.Exists)
                {
                    logger.Logging($"Директория для сканирования не определена!");
                    return;
                }

                controller.Directory = BrowseDataContext.SelectDirectory;

                logger.Logging($"Запуск сканирования выбранной директории: [ { controller.Directory.Name } ]");
                controller.Run();
            });

            ExplodeCommand = new DelegateCommand(() => controller.Explode());
        }

        public BrowseViewModel BrowseDataContext { get; }

        public LogViewModel LogDataContext { get; }

        public BackPanelViewModel BackDataContext { get; }

        public SelectTypeFileViewModel SelectTypeFileDataContext { get; }

        public DelegateCommand RunScanCommand { get; }

        public DelegateCommand ExplodeCommand { get; }
    }
}