﻿using System.Collections.Generic;
using System.IO;

namespace DrawingsControllSystem.Model.Common
{
    public class FindDirectory
    {
        /// <summary>
        /// Получение или установка наименование найденной директории
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Получение или установка списка файлов, переведенные в объекты <see cref="Drawing"/>, найденые в текущей директории
        /// </summary>
        public List<Drawing> Files { get; set; }

        /// <summary>
        /// Получение или установка списка директорий находящихся в текущей директории
        /// </summary>
        public List<FindDirectory> Subfolders { get; set; }
    }
}