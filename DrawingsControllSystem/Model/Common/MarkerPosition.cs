﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace DrawingsControllSystem.Model.Common
{
    [DataContract]
    [JsonConverter(typeof(StringEnumConverter))]
    public enum MarkerPosition
    {
        [EnumMember(Value = "Center")]
        Center,

        [EnumMember(Value = "TopLeft")]
        TopLeft,

        [EnumMember(Value = "TopRight")]
        TopRight,

        [EnumMember(Value = "BottomLeft")]
        BottomLeft,

        [EnumMember(Value = "BottomRight")]
        BottomRight,

        [EnumMember(Value = "Top")]
        Top,
    }
}