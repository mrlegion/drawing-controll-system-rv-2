﻿using System;
using System.IO;
using DrawingsControllSystem.Common;
using DrawingsControllSystem.Common.MVVM;
using DrawingsControllSystem.Model.Impliments;
using DrawingsControllSystem.Model.Interfaces;

namespace DrawingsControllSystem.Model.Common
{
    public abstract class BaseController : BindableBase
    {
        #region Fields

        // Компоненты
        protected readonly ILogger _logger;
        protected readonly IStoragekeeper _storagekeeper;
        protected readonly IManager _manager;

        // Приватные поля
        protected DirectoryInfo _directory;
        protected FileExtansion _extansion;

        #endregion

        protected BaseController(ILogger logger, IStoragekeeper storagekeeper, IManager manager)
        {
            _logger = logger;
            _storagekeeper = storagekeeper;
            _manager = manager;

            _logger.PropertyChanged += (sender, args) => RaisePropertyChanged(args.PropertyName);
        }

        #region Properties

        public DirectoryInfo Directory
        {
            get => _directory;
            set => SetProperty(ref _directory, value);
        }

        public FileExtansion Extansion
        {
            get => _extansion;
            set
            {
                SetProperty(ref _extansion, value);
                _logger.Logging($"Задание нового типа отслеживания файлов: [ {value.ToString()} ]");
            }
        }

        public string Log => _logger.Log;

        #endregion

        #region Public Methods

        public virtual void Run()
        {
            throw new NotImplementedException();
        }

        public virtual void Stop()
        {
            throw new NotImplementedException();
        }

        public virtual void SafeLogToFile()
        {
            _logger.SaveToFile(Directory);
        }

        public virtual void ChangeFolder()
        {
            throw new NotImplementedException();
        }

        public virtual void Explode()
        {
            _logger.Logging("Запуск опции деления по форматам");
            _manager.ExplodeAsync(Directory);
        }

        public virtual void ShowInformation()
        {
            _manager.ShowInformation();
        }

        public virtual void Logging(string message)
        {
            _logger.Logging(message);
        }

        #endregion
    }
}