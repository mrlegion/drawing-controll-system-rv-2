﻿namespace DrawingsControllSystem.Model.Common
{
    public class Size
    {
        public float Width { get; set; }
        public float Height { get; set; }
        public float Area { get; set; }

        public Size(float width, float height)
        {
            Width = width;
            Height = height;

            Area = Width * Height;
        }
    }
}