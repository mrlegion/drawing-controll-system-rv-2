﻿namespace DrawingsControllSystem.Model.Common
{
    /// <summary>
    /// Information for format
    /// </summary>
    public class Format
    {
        /// <summary>
        /// Gets format name
        /// </summary>
        public string Name { get; }

        public Size Size { get; }

        /// <summary>
        /// Gets a low level of the area for the format
        /// </summary>
        public float LowArea { get; }

        /// <summary>
        /// Gets a up level of the area for the format
        /// </summary>
        public float UpArea { get; }

        /// <summary>
        /// Gets value includes standart formats (a4)
        /// </summary>
        public double Includes { get; }

        public int PermissibleVariationArea { get; }

        /// <summary>
        /// Inizialize new format object
        /// </summary>
        /// <param name="name">Format name</param>
        /// <param name="size"></param>
        /// <param name="includes">Includes standart formats</param>
        /// <param name="permissibleVariationArea"></param>
        public Format(string name, Size size, double includes, int permissibleVariationArea)
        {
            Name = name;
            Size = size;
            Includes = includes;
            PermissibleVariationArea = permissibleVariationArea;

            // set low and up area value
            LowArea = Size.Area * (100 - PermissibleVariationArea) / 100;
            UpArea = Size.Area * (100 + PermissibleVariationArea) / 100;
        }
    }
}