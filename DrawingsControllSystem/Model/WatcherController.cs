﻿using System.IO;
using DrawingsControllSystem.Common;
using DrawingsControllSystem.Model.Common;
using DrawingsControllSystem.Model.Impliments;
using DrawingsControllSystem.Model.Interfaces;

namespace DrawingsControllSystem.Model
{
    public class WatcherController : BaseController
    {
        private readonly IWatcher _watcher;

        public WatcherController(IWatcher watcher, ILogger logger, IStoragekeeper storagekeeper, IManager manager) 
            : base(logger, storagekeeper, manager)
        {
            _watcher = watcher;
        }

        public override void Run()
        {
            if (Extansion == FileExtansion.None)
                Extansion = FileExtansion.Tiff;
            _watcher.Run(Directory, Extansion);
        }

        public override void Stop()
        {
            _watcher.Stop();
        }

        public override void ChangeFolder()
        {
            _logger.Logging("Принудительная смена папки вызванная пользователем");

            if (_storagekeeper.Count != 0)
            {
                _storagekeeper.SendToBank(Directory);
                _logger.Logging("Принудительная смена папки завершена успешна");
                return;
            }

            if (_storagekeeper.Count == 0)
            {
                _logger.Logging("Принудительная смена папки прервана! В коллекции нет ни одного файла");
                return;
            }

            _logger.Logging("Принудительная смена папки прервана! Неизвестная ошибка!");
        }
    }
}