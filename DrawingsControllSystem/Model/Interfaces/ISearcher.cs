﻿using System.IO;
using DrawingsControllSystem.Model.Common;

namespace DrawingsControllSystem.Model.Interfaces
{
    public interface ISearcher
    {
        void Search(DirectoryInfo directory, FileExtansion extansion);
        void SearchAsync(DirectoryInfo directory, FileExtansion extansion);
    }
}