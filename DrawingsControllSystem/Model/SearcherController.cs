﻿using DrawingsControllSystem.Common;
using DrawingsControllSystem.Model.Common;
using DrawingsControllSystem.Model.Interfaces;

namespace DrawingsControllSystem.Model
{
    public class SearcherController : BaseController
    {
        private readonly ISearcher _searcher;

        public SearcherController(ILogger logger, IStoragekeeper storagekeeper, IManager manager, ISearcher searcher) :
            base(logger, storagekeeper, manager)
        {
            _searcher = searcher;
        }

        public override void Run()
        {
            _searcher.SearchAsync(Directory, Extansion);
        }


    }
}