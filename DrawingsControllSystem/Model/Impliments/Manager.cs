﻿using System.Collections.Generic;
using System.IO;
using DrawingsControllSystem.Model.Common;
using DrawingsControllSystem.Model.Interfaces;

namespace DrawingsControllSystem.Model.Impliments
{
    public class Manager : IManager
    {
        private readonly Dictionary<string, List<Drawing>> _data;
        private readonly ILogger _logger;
        private readonly IExploder _exploder;
        private readonly IStatistics _statistics;

        public Manager(ILogger logger, IExploder exploder, IStatistics statistics)
        {
            this._logger = logger;
            this._exploder = exploder;
            this._statistics = statistics;
            _data = new Dictionary<string, List<Drawing>>();
        }

        // Todo: А нужно ли это?
        public Dictionary<string, List<Drawing>> Data => _data;

        public void Add(string name, List<Drawing> drawings)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name))
            {
                _logger.Logging($"Имя главы не может быть пустым");
                return;
            }

            if (drawings == null || drawings.Count == 0)
            {
                _logger.Logging($"Коллекция чертежей пустая или Null");
                return;
            }

            if (_data.ContainsKey(name))
            {
                _data[name].AddRange(drawings);
                return;
            }

            _data.Add(name, new List<Drawing>(drawings));
            _logger.Logging($"Папка [ {name} ] добавлена в хранилище");
        }

        public void Explode(DirectoryInfo source)
        {
            _exploder.Explode(source, Data);
        }

        public void ExplodeAsync(DirectoryInfo source)
        {
            _exploder.ExplodeAsync(source, _data);
        }

        public void ShowInformation()
        {
            _statistics.ShowInformation(Data);
        }
    }
}