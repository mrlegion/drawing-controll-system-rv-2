﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DrawingsControllSystem.Model.Common;
using DrawingsControllSystem.Model.Interfaces;
using Microsoft.Practices.Unity.InterceptionExtension.Configuration;
using Unity.Interception.Utilities;

namespace DrawingsControllSystem.Model.Impliments
{
    public class Searcher : ISearcher
    {
        private readonly ICreator _creator;
        private readonly ILogger _logger;
        private readonly IStoragekeeper _storagekeeper;
        private readonly IManager _manager;

        private FileExtansion _extansion;
        private DirectoryInfo _sourceDirectory;

        public Searcher(ICreator creator, ILogger logger, IStoragekeeper storagekeeper, IManager manager)
        {
            _creator = creator;
            _logger = logger;
            _storagekeeper = storagekeeper;
            _manager = manager;
        }

        public void Search(DirectoryInfo directory, FileExtansion extansion)
        {
            _extansion = extansion;
            _sourceDirectory = new DirectoryInfo(Path.Combine("Search found temp", directory.FullName));

            SearchDirectory(directory);
        }

        public async void SearchAsync(DirectoryInfo directory, FileExtansion extansion)
        {
            await Task.Factory.StartNew(() => Search(directory, extansion));
            _logger.Logging("Сканирование директории завершенно!");
        }

        private void SearchDirectory(DirectoryInfo directory)
        {
            var files = directory.GetFiles("*.*", SearchOption.TopDirectoryOnly).Where(file => file.CheckFileExtansion(_extansion)).ToList();
            if (files.Count > 0)
            {
                foreach (FileInfo file in files)
                {
                    _creator.Create(file, false);
                }
            }

            if (_storagekeeper.Count != 0)
            {
                _logger.Logging($"Папка [ { directory.Name } ] внесена в склад.");
                _logger.Logging($"Запуск процедуры смены папки!");
                _manager.Add(directory.Name, _storagekeeper.Drawings);
                _storagekeeper.Clear();
            }

            var directories = directory.GetDirectories("*.*", SearchOption.TopDirectoryOnly).ToList();
            if (directories.Count > 0)
            {
                foreach (DirectoryInfo directoryInfo in directories)
                {
                    // Todo: Сделать возможность контролировать данный процесс
                    // Если найденная директория - это директория деления, то ее стоит пропустить
                    if (directoryInfo.Name == "Exploder Directory" || directoryInfo.Name == "Папка деления")
                        continue;

                    SearchDirectory(directoryInfo);
                }
            }
        }
    }
}