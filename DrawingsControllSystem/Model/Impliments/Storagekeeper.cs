﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using DrawingsControllSystem.Model.Common;
using DrawingsControllSystem.Model.Interfaces;

namespace DrawingsControllSystem.Model.Impliments
{
    public class Storagekeeper : IStoragekeeper
    {
        private readonly List<Drawing> _drawings;
        private readonly IFolderCreator _folderCreator;
        private readonly IFileCarrier _carrier;
        private readonly IManager _manager;
        private readonly ILogger _logger;

        public int Count => _drawings.Count;

        /// <summary>
        ///   Инициализирует новый экземпляр класса <see cref="Storagekeeper" />.
        /// </summary>
        public Storagekeeper(ILogger logger, IFolderCreator folderCreator, IFileCarrier carrier, IManager manager)
        {
            this._logger = logger;
            this._folderCreator = folderCreator;
            this._carrier = carrier;
            this._manager = manager;
            _drawings = new List<Drawing>();
        }

        public List<Drawing> Drawings => _drawings;

        public void Add(Drawing item)
        {
            Add(item, false);
        }

        public void Add(Drawing item, bool replace)
        {
            if (replace)
                _drawings.RemoveAt(Count - 1);

            _drawings.Add(item);

            _logger.Logging($"Файл [ {item.Path.Name} ] добавлен в коллекцию");
            _logger.Logging($"Формат файла: {item.Format}");
            _logger.Logging($"Общее количество файлов в коллекции: {Count}");
        }

        public void Clear()
        {
            _drawings.Clear();
            _logger.Logging("Коллекция чертежей очищенна.");
        }

        public void SendToBank(DirectoryInfo source)
        {
            _folderCreator.Create(source);

            if (_carrier.Transfer(_folderCreator.Directory, _drawings))
            {
                _manager.Add(_folderCreator.Directory.Name, _drawings);
                Clear();
            }
        }
    }
}