﻿using System.Windows.Controls;
using DrawingsControllSystem.Common;
using DrawingsControllSystem.Common.MVVM;
using DrawingsControllSystem.ViewModels;
using DrawingsControllSystem.Views;
using Unity;

namespace DrawingsControllSystem
{
    public class ShellViewModel : BindableBase
    {
        /// <summary>
        /// Заголовок основного окна
        /// </summary>
        private string _title = "Drawings Controll System";

        /// <summary>
        /// Получение или установка нового заголовка окна
        /// </summary>
        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        /// <summary>
        /// Выбраная текущая страница отображения
        /// </summary>
        private Page _current;

        /// <summary>
        /// Получение или установка страницы отображения в главном окне
        /// </summary>
        public Page Current
        {
            get => _current;
            set => SetProperty(ref _current, value);
        }

        public ShellViewModel(IUnityContainer container)
        {
            Current = container.Resolve<SelectView>();
            Current.DataContext = container.Resolve<SelectViewModel>();

            // Удалить после настройки внешнего вида таблицы
            // Current = container.Resolve<InfoView>();
            // Current.DataContext = container.Resolve<InfoViewModel>();
        }
    }
}