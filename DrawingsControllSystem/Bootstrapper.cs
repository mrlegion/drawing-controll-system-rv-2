﻿using System.Windows;
using System.Windows.Controls;
using DrawingsControllSystem.Common;
using DrawingsControllSystem.HelpersD;
using DrawingsControllSystem.Model;
using DrawingsControllSystem.Model.Common;
using DrawingsControllSystem.Model.Impliments;
using DrawingsControllSystem.Model.Interfaces;
using DrawingsControllSystem.UserComponents;
using DrawingsControllSystem.ViewModels;
using DrawingsControllSystem.ViewModels.UserComponents;
using DrawingsControllSystem.Views;
using Unity;
using Unity.Injection;

namespace DrawingsControllSystem
{
    public class Bootstrapper
    {
        /// <summary>
        /// Основной IoC-контейнер
        /// </summary>
        private readonly IUnityContainer _container;

        /// <summary>
        /// Инициализация нового экземпляра класса <see cref="Bootstrapper"/>
        /// </summary>
        public Bootstrapper()
        {
            _container = new UnityContainer();
        }

        /// <summary>
        /// Создание оболочки приложения
        /// </summary>
        private void CreateShell()
        {
            _container.RegisterSingleton<Shell>();
            _container.RegisterSingleton<ShellViewModel>();
        }

        /// <summary>
        /// Инициализация оболочки приложения
        /// </summary>
        private void InitializeShell()
        {
            Shell shell = _container.Resolve<Shell>();
            shell.DataContext = _container.Resolve<ShellViewModel>();
            Application.Current.MainWindow = shell;
            Application.Current.MainWindow?.Show();
            Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;
        }

        /// <summary>
        /// Регистрация страниц приложения для оболочки
        /// </summary>
        private void RegisterPage()
        {
            // Выбор типа программы
            _container.RegisterSingleton<SelectView>();
            _container.RegisterSingleton<SelectViewModel>();

            // Тип отслеживания
            _container.RegisterSingleton<WatcherView>();
            _container.RegisterSingleton<WatcherViewModel>();

            // Тип поиска по выбранному пути
            _container.RegisterSingleton<SearcherView>();
            _container.RegisterSingleton<SearcherViewModel>();
        }

        /// <summary>
        /// Регистрация пользовательских компонентов интерфейса программы
        /// </summary>
        private void RegisterUserComponents()
        {
            // Компонент выбора папки
            _container.RegisterType<BrowseViewModel>();
            _container.RegisterType<BrowseUserComponent>();

            // Компонент вывода информации журнала событий
            _container.RegisterType<LogViewModel>();
            _container.RegisterType<LogUserComponent>();

            // Компонент позволяющий вернуться назад к меню
            _container.RegisterType<BackPanelViewModel>();
            _container.RegisterType<BackPanelUserComponent>();

            // Компонент выбора типа файла
            _container.RegisterType<SelectTypeFileViewModel>();
            _container.RegisterType<SelectFileTypeUserComponent>();

            // Компонент вывода информации
            _container.RegisterType<InfoViewModel>();
            _container.RegisterType<InfoView>();
        }

        /// <summary>
        /// Регистрация модулей бизнес-логики приложения
        /// </summary>
        private void RegisterModules()
        {
            _container.RegisterSingleton<IWatcher, Watcher>();
            _container.RegisterSingleton<ISearcher, Searcher>();
            _container.RegisterType<ICreator, Creator>();
            _container.RegisterSingleton<ILogger, Logger>();
            _container.RegisterSingleton<IStoragekeeper, Storagekeeper>();
            _container.RegisterType<IExploder, Exploder>();
            _container.RegisterSingleton<IStatistics, Statistics>();
            _container.RegisterType<IFileCarrier, FileCarrier>();
            _container.RegisterSingleton<IManager, Manager>();
            _container.RegisterType<IFolderCreator, FolderCreator>();

            _container.RegisterType<Pages>();

            _container.RegisterType<BaseController, WatcherController>("WatcherController");
            _container.RegisterType<BaseController, SearcherController>("SearchController");


        }

        /// <summary>
        /// Регистрация модулей помощи для бизнес-логики приложения
        /// </summary>
        private void RegisterHelpers()
        {
            _container.RegisterType<CreateStrategy, TiffCreatorStrategy>();
        }

        private void RegisterDebugHelpers()
        {
            _container.RegisterType<DummyListCreator>();
        }

        /// <summary>
        /// Запуск инициализации настроики класса <see cref="Bootstrapper"/>
        /// </summary>
        public void Run()
        {
            RegisterUserComponents();
            RegisterPage();
            CreateShell();
            InitializeShell();
            RegisterHelpers();
            RegisterModules();
        }
    }
}